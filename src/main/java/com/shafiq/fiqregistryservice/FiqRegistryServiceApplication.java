package com.shafiq.fiqregistryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class FiqRegistryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FiqRegistryServiceApplication.class, args);
	}

}
